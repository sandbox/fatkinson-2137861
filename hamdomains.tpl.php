<?php
/**
 * @file
 * Module to provide the code for the hamdomains table.
 */

  $n = 0;
  $username = isset($variables['list_array']['username']) ? strtoupper($variables['list_array']['username']) : '';
  $version = isset($variables['list_array']['version']) ? $variables['list_array']['version'] : '';
  $bgcolor = isset($variables['list_array']['bgcolor']) ? $variables['list_array']['bgcolor'] : '';
  $border = isset($variables['list_array']['border']) ? $variables['list_array']['border'] . 'px' : '';
  $number = isset($variables['list_array']['number']) ? $variables['list_array']['number'] : '';
  $callarray = isset($variables['list_array']['result_array']) ? $variables['list_array']['result_array'] : '';
  $columns = isset($variables['list_array']['columns']) ? $variables['list_array']['columns'] : '';
  echo "  <font face=\"times new roman\">\n";
  echo "  <!-- Original hamdomain script written by WB4AEJ!      -->\n";
  echo "  <!-- Modified into Drupal module by [sara0611988]!   -->\n";

  echo "  <!-- User '$username' owns this site!                  -->\n";
  echo "  <!-- Hamdomain module version $version for Drupal 7!   -->\n";
  echo "  <center>\n";
  echo "    <table bgcolor=\"$bgcolor\" cellPadding=2 style=\"border: $border solid\">\n";
  echo "      <tr>\n";

  while ($n < $number) :
    $hold = explode('&', $callarray[$n]);
    $call = hamdomains_zero_convert($hold[1]);
    $scode = $hold[0];
    $url = $hold[2];
    echo "        <td><p align=center>\n";
    echo "          <a href=\"http://";
    echo "$url";
    echo "\" target=\"_blank\">$call</a>\n";
    echo "        </td>\n";
    if (($n % $columns == $columns - 1) && ($n != $number - 1)) :
      echo "      </tr>\n";
      echo "      <tr>\n";
    endif;
  $n++;
  endwhile;
  echo "      </tr>\n";
  echo "    </table>\n";
  echo "  </center>\n";
  echo "  </font>\n";
  echo "  <!-- Copyright 2013 by WB4AEJ -->\n";
?>
