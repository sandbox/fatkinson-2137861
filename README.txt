The 'Ham Domains' module is for Amateur Radio Operators that would like to
display random links to other ham radio domains on their Drupal 7 Web sites.
'Ham domains' connects to a database of ham radio URLs and displays them
randomly in a customizable table with call signs and links to the associated
Web sites.  They are always sorted Callbook style (1st by region code 0-9, then
by suffix, then by prefix).

To install and configure the Ham Domains module, take the following steps:

1. Configure the temporary file directory field [if you haven't already] as an
absolute path to the temporary directory.  In admin mode, select
'Configuration'.  Under 'Media', click on 'File System'. Whatever the absolute
path is to your public HTML directory is (example: home/yourname/public_html),
enter the absolute path plus /tmp into the 'Temporary Directory' field
(example: home/yourname/public_html/tmp).
The install will not complete unless this setting has been made.

2. Install the Ham Domains block module (see below) onto your Web site.

3. Sign up for a user name and password to enter into the module configuration
(your username should be your call sign in all lower case letters).  I will
approve your password in a short time.  Be patient.

4. Once you get your user name and password, go into the module configuration 
(click on 'Modules' then scroll down to 'Hamdomains' and click on 'Configure').
Enter your user name and password in the appropriate fields.

5. Configure the number of rows (up to three) and columns (up to ten) and
change the cell background color [if desired].  By default, the columns are set
at two rows and eight columns.  The background color is set at '292929' (to 
match the Bartik background color).

6. Go to 'Structure', then 'Blocks'.  Set the Region setting for 'Ham Domains'
(I usually set it to 'Header') and click on the 'Save Blocks' button at the end
of the page.

7. Click on 'People' and then click on the 'Permissions' tab. Scroll down to
'Hamdomains' and set your permissions.  Usually, you click on the checkboxes 
for the 'Authenticated User' and the 'Anonymous User' tabs or whatever your
preferences would be.  Then scroll to the bottom of the page and click on 'Save
Permissions' button.

You should be good to go.  

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

You should see a table with links to ham radio sites on your Web site.  

The links should be different every time the page/block is reloaded.  

There is an issue with the display of the title (if you enable it by deleting
the '<none>' tag in the block title field [or putting your own title in the
block title field] on the Ham Domains configuration page) when the module is
displayed in the header block.  The problem is not with the module itself.
The problem is with the CSS files in Drupal.  I may post it as an issue and 
see if the Drupal developers fix it.  But the title does seem to show up when
the module is moved to other blocks.  

The Web site where I developed the Ham Domains database can be seen at Your
Own Ham Domain.  The content of that article is somewhat dated but you can see
the entire contents of the Ham Domain database displayed on that page. 

If you have a ham domain (your callsign dot com, net, etc., like www.n5bl.org),
feel free to submit it for addition to the Ham Domains database.  I love adding
U.S. and DX call sign domains to this database so please submit those and 
especially the ones with foreign top level domains (like .co.uk or any others)
for inclusion in the Ham Domain database.   It isn't necessary that they be in
English as long as it can be clearly seen that the content is about Amateur
Radio.

If you don't already have a Ham Domain registered, read 'Your Own Ham Domain'
and get yourself started. But instead of writing the code for your own pages,
install Drupal on it.  I hope to rewrite it so that they install Drupal 
instead of writing a Web page of their own.

I reserve the right to terminate any user's username and password who
improperly uses or abuses my database.  Please do not put my module on any Web
sites with objectionable materials.  And please do not share your 'Ham Domains'
username or password to my database with anyone else.  They can get one for 
themselves by applying for one online.  All accounts issued for 'Ham Domains' 
will be read only.

I also ask that you not modify my script in any way as your modifications might
have an adverse affect on my database.

If you are running a non-Drupal Web site with PHP capability and wish to obtain
a copy of the original PHP script I wrote, contact me and I will send it to you.
  You will still need a user name and password to access my database.

I wrote the original scripting to display these domains in a table on a PHP
page.  But I won't take credit for converting it into a Drupal module because 
I had help from a Freelancer [using the ID of sara0611988 on Freelancer.com].
She took my script and rewrote it into a Drupal 7 module for me.

The script will not show error messages [unless you are logged on as an
administrator]. All error messages are suppressed in the interest of keeping
your Web page looking nice.

It would be great to convert this to a Drupal 8 module since Drupal 8 is about
to be released.  If someone could help me convert it, I would be very grateful.

I hope to develop my PHP skills to where I can write modules for Drupal on my
own. And I intend to see everyone at Drupalcon 2014 in Austin this coming June.

Thanks to Lucas (irc: heddn), irc: superspring, irc: goedsole, and my friend 
Charles Combs for their patience while coaching me in revising the pareview.sh
errors and warnings that were flagged during the final debugging of the 
hamdomains module.  

Copyright © 2014 by WB4AEJ.  

Contact WB4AEJ via http://www.wb4aej.com/contact.php 

